\section{Related work and future directions}

Concepts related to quantized nonparametric
estimation appear in multiple communities.
As mentioned in the introduction, Donoho's 1997 Wald Lectures
(on the eve of the 50th anniversary of Shannon's 1948 paper),
drew sharp parallels between rate distortion, metric
entropy and minimax rates, focusing on the same Sobolev
function spaces we treat here.  
%A closely related information-theoretic formulation of minimax rates
%is the well known correspondence between metric entropy and
%minimax rates through the ``le Cam equation'' $H_\epsilon(\F) = n
%\epsilon^2$ \cite{Wong:Shen:1995,Yang:Barron:1999}.
One view of the present
work is that we take this correspondence further by
studying how the risk continuously degrades with the level of
quantization.  We have analyzed 
the precise leading order asymptotics for quantized regression
over the Sobolev spaces, showing that these rates
and constants are realized with coding schemes 
that are adaptive to the smoothness $m$ and radius $c$ of
the ellipsoid, achieving automatically the optimal rate
for the regime corresponding to those parameters given the specified
communication budget.  Our detailed analysis is possible
due to what Nussbaum \cite{nussbaum1999minimax}
calls the ``Pinsker phenomenon,'' 
refering to the fact that linear filters attain the minimax rate in the
over-sufficient regime.  It will be interesting
to study quantized nonparametric estimation in cases where the Pinsker
phenomenon does not hold, for example over Besov bodies
and different $L_p$ spaces.

Many problems of rate distortion type are similar to quantized
regression.  The standard ``reverse water filling'' construction to
quantize a Gaussian source with varying noise levels plays a key role
in our analysis, as shown in Section~\ref{sec:lowerbound}.  In our
case the Sobolev ellipsoid is an infinite Gaussian sequence model,
requiring truncation of the sequence at the appropriate level
depending on the targeted quantization and estimation error.  In the
case of Euclidean balls, Draper and Wornell \cite{draper2004side}
study rate distortion problems motivated by communication in sensor
networks; this is closely related to the problem of quantized minimax
estimation over Euclidean balls that we analyzed in
\cite{zhu2014quantized}.  The essential difference between rate
distortion and our quantized minimax framework is that in rate
distortion the quantization is carried out for a random source, while
in quantized estimation we quantize our estimate of the deterministic
and unknown basis coefficients.  Since linear estimators are
asymptotically minimax for Sobolev spaces under squared error (the
``Pinsker phenomenon''), this naturally leads to an alternative view
of quantizing the observations, or said differently, of compressing
the data before estimation.

Statistical estimation from compressed data has appeared previously in
different communities.  In \cite{zhou:09} a procedure is analyzed that
compresses data by random linear transformations in the setting of sparse linear
regression. Zhang and Berger \cite{zhangberger} study estimation
problems when the data are communicated from multiple sources; Ahlswede
and Csisz\'ar \cite{ahlswede:86} consider testing problems under
communication constraints; the use of 
side information is studied by Ahlswede and Burnashev
\cite{ahlswede:90}; other formulations in terms of multiterminal
information theory are given by Han and Amari \cite{hanamari};
nonparametric problems are considered by Raginsky in
\cite{raginsky:07}.
In a distributed setting the data may be divided across different
compute nodes, with distributed estimates then aggregated or
pooled by communicating with a central node.
The general ``CEO problem'' of distributed estimation was introduced by
Berger, Zhang and Viswanathan \cite{ceo}, and has been 
recently studied in parametric settings
in \cite{zhang2013information,garg:14}.  
These papers take the view that the data are communicated
to the statistician at a certain rate, which may introduce
distortion, and the goal is to study the degradation
of the estimation error.  In contrast, in our setting we can view
the unquantized data as being fully available to the statistician
at the time of estimation, with communication constraints
being imposed when communicating the estimated model 
to a remote location.  


Finally, our quantized minimax analysis shows achievability using
random coding schemes, which are not computationally efficient.  A
natural problem is to develop practical coding schemes that come close
to the quantized minimax lower bounds.  In our view, the most promising approach
currently is to exploit source coding schemes based on greedy sparse
regression~\cite{venkataramanan2013lossy}, applying such techniques
blockwise according to the procedure we developed in
Section~\ref{sec:achievability}.

