\section{Quantized estimation over Sobolev spaces} \label{sec:lowerbound}
\def\F{{\mathcal F}}
\def\cE{{\mathcal E}}
\def\cQ{{\mathcal Q}}

Recall that the \emph{Sobolev space of order $m$ and radius $c$} is defined by
\begin{align*}
W(m,c)=\Big\{&f\in [0,1]\to\mathbb R:f^{(m-1)} \text{ is absolutely continuous and }\\
&\int_0^1(f^{(m)}(x))^2dx\leq c^2\Big\}.
\end{align*}
The \emph{periodic Sobolev space} is defined by
\begin{equation}\label{eqn_persobolev}
\tilde W(m,c)=\left\{f\in W(m,c):f^{(j)}(0)=f^{(j)}(1),\ j=0,1,\dots,m-1\right\}.
\end{equation}
The white noise model is equivalent to making $n$
equally space observations along the sample path, $Y_i = f(i/n) +
\sigma \epsilon_i$, where $\epsilon_i \sim {\mathcal N}(0,1)$
\cite{brown:low:94}.  In this formulation, the noise level scales as
$\epsilon^2 = \sigma^2/n$, and the rate of convergence takes the
familiar form $n^{-\frac{2m}{2m+1}}$ where $n$ is the number of
observations.

To carry out quantized estimation we now require
an encoder
\begin{equation*}
\phi_\eps:\mathbb R^{[0,1]}\to\{1,2,\dots,2^{B_\eps}\}
\end{equation*}
which is a function applied to the sample path $X(t)$.
The decoding function then takes the form
\begin{equation*}
\psi_\eps:\{1,2,\dots,2^{B_\eps}\}\to\mathbb R^{[0,1]}
\end{equation*}
and maps the index to a function estimate.
As in the previous section, we write the composition of the encoder
and the decoder as 
$\hat f_\eps=\psi_\eps\circ\phi_\eps$, which we call the quantized estimator.
The communication or storage $C(\hat f_\eps)$ required by this quantized estimator
is no more than $B_\eps$ bits.  

To recast quantized estimation in terms of
an infinite sequence model, let
let $({\varphi_j})_{j=1}^\infty$ be the trigonometric basis, and let
\begin{equation*}
\theta_j=\int_0^1 \varphi_j(t)f(t)dt,\quad j=1,2,\dots,
\end{equation*}
be the Fourier coefficients.
It is well known \cite{tsybakov:2008} that $f=\sum_{j=1}^\infty\theta_j\varphi_j$ belongs to $\tilde W(m,c)$ if and only if 
the Fourier coefficients $\theta$ belong to the \emph{Sobolev ellipsoid} defined as
\begin{equation}\label{eqn_sobolevellip}
\Theta(m,c)=\left\{\theta\in\ell_2: \sum_{j=1}^\infty a_j^2\theta_j^2\leq \frac{c^2}{\pi^{2m}}\right\}
\end{equation}
where 
\begin{equation*}
a_j=\begin{cases}
j^m,&\text{for even } j,\\
(j-1)^m,&\text{for odd } j.
\end{cases}
\end{equation*}
Although this is the standard definition of a Sobolev ellipsoid, 
for the rest of the paper 
we will set $a_j=j^m$, $j=1,2,\dots$ for convenience of analysis.
All of the results hold for both definitions of $a_j$.
Also note that (\ref{eqn_sobolevellip}) actually gives a more general definition,
since $m$ is no longer assumed to be an integer, as it is in (\ref{eqn_persobolev}).
Expanding with respect to the same orthonormal basis, the observed path
$X(t)$ is converted into an infinite Gaussian sequence
\begin{equation*}
Y_j=\int_0^1 \varphi_j(t) \,dX(t),\quad j=1,2,\dots,
\end{equation*}
with $Y_j\sim \mathcal N(\theta_j,\varepsilon^2)$. 
For an estimator $(\hat\theta_j)_{j=1}^\infty$ of $(Y_j)_{j=1}^\infty$, 
an estimate of $f$ is obtained by
\begin{equation*}
\hat f(x)=\sum_{j=1}^\infty \hat\theta_j\varphi_j(x)
\end{equation*}
with squared error $\|\hat f-f\|_2^2=\|\hat\theta-\theta\|_2^2$.
In terms of this standard reduction, the quantized minimax risk is thus reformulated as
\begin{equation}
R_\eps(m,c,B_\eps)=\inf_{\hat\theta_\eps,C(\hat\theta_\eps)\leq B_\eps}\sup_{\theta\in\Theta(m,c)}\E_\theta\|\theta-\hat\theta_\eps\|_2^2.\label{minimaxriskdef}
\end{equation}


To state our result, we need to define the value of the following
variational problem:
\begin{align}\label{var7}
& {\mathsf V}_{m,c,d}  \triangleq \\
\nonumber & \max_{(\sigma^2,x_0) \in
  \F(m,c,d)} \int_0^{x_0}\frac{\sigma^2(x)}{\sigma^2(x)+1}dx 
+ x_0\exp\left(\frac{1}{x_0}\int_0^{x_0}\log\frac{\sigma^4(x)}{\sigma^2(x)+1}dx-\frac{2d}{x_0}\right)
\end{align}
where the feasible set $\F(m,c,d)$ is the collection of increasing functions $\sigma^2(x)$ and
values $x_0$ satisfying
\begin{gather*}
\int_0^{x_0} x^{2m}\sigma^2(x)dx\leq c^2 \\
\frac{\sigma^4(x)}{\sigma^2(x)+1}\geq
\exp\left(\frac{1}{x_0}\int_0^{x_0}\log\frac{\sigma^4(x)}{\sigma^2(x)+1}dx-\frac{2d}{x_0}\right)
\text{ for all }x\leq x_0.
\end{gather*}
The significance and interpretation of the variational problem will
become apparent as we outline the proof of this result.

\clearpage
\begin{theorem}
Let $R_\eps(m,c,B_\eps)$ be defined as in
(\ref{minimaxriskdef}), for $m>0$ and $c>0$.
\begin{enumerate}[(i)]
\item 
If $B_\varepsilon\varepsilon^{\frac{2}{2m+1}}\to\infty$ as $\varepsilon\to0$, then
\begin{equation*}
\liminf_{\varepsilon\to 0} \varepsilon^{-\frac{4m}{2m+1}}R_\varepsilon(m,c,B_\varepsilon)\geq \mathsf P_{m,c}
\end{equation*}
where $\mathsf P_{m,c}$ is Pinker's constant defined in
(\ref{pinsker}).
\vskip15pt
\item If $B_\varepsilon\varepsilon^{\frac{2}{2m+1}}\to d$ for some constant $d$ as $\varepsilon\to0$, then
\begin{equation*}
\liminf_{\varepsilon\to 0}
\varepsilon^{-\frac{4m}{2m+1}}R_\varepsilon(m,c,B_\varepsilon)\geq
\mathsf P_{m,c}+\mathsf Q_{m,c,d} = {\mathsf V}_{m,c,d}
\end{equation*}
where $\mathsf V_{m,c,d}$ is the value of the variational problem \eqref{var7}.
\vskip15pt
\item If $B_\varepsilon\varepsilon^{\frac{2}{2m+1}}\to0$ and $B_\varepsilon\to\infty$ as $\varepsilon\to 0$, then
\begin{equation*}
\liminf_{\varepsilon\to 0}B_\varepsilon^{2m}R_\varepsilon(m,c,B_\varepsilon)\geq \frac{c^2m^{2m}}{\pi^{2m}}.
\end{equation*}
\end{enumerate}
\label{thm_lowerbound}
\end{theorem}

In the first regime where the number of bits $B_\varepsilon$ is much
greater than $\varepsilon^{-\frac{2}{2m+1}}$, we recover the same
convergence result as in Pinsker's theorem, in terms of both
convergence rate and leading constant. The proof of the lower bound
for this regime can directly follow the proof of Pinsker's theorem, since
the set of estimators considered in our minimax framework is a subset of
all possible estimators.

In the second regime where we have ``just enough''
bits to preserve the rate, we suffer a loss in terms of the leading
constant. In this ``Goldilocks regime,'' the optimal rate
$\varepsilon^{-\frac{4m}{2m+1}}$ is achieved but the constant in front
of the rate is Pinsker's constant $\mathsf P_{m,c}$ plus a positive
quantity $\mathsf Q_{m,c,d}$ determined by
the variational problem. 

While the solution to this variational problem does not appear to have an explicit
form, it can be computed numerically.   We discuss this term at length
in the sequel, where we explain the origin of the variational problem,
compute the constant numerically and approximate it from above and
below. The constants $\mathsf P_{m,c}$ and $\mathsf Q_{m,c,d}$ are
shown graphically in Figure \ref{fig:constant}.
Note that the parameter $d$ can be thought of as
the average number of bits per coefficient used by an optimal
quantized estimator, since
$\varepsilon^{-\frac{2}{2m+1}}$ is asymptotically the number
of coefficients needed to estimate at the classical minimax rate.
As shown in Figure~\ref{fig:constant}, the constant
for quantized estimation quickly approaches the 
Pinsker constant as $d$ increases---when $d=3$ the
two are already very close.


\begin{figure}[t]
\begin{center}
\begin{tabular}{c}
\hskip35pt
\includegraphics[width=.85\textwidth]{figs/pzconstant} \\
\end{tabular}
\caption{The constants $\mathsf P_{m,c} + \mathsf Q_{m,c,d}$ as a
  function of quantization level $d$ in the sufficient regime,
  where $B_\varepsilon\varepsilon^{\frac{2}{2m+1}}\to d$. 
  The parameter $d$ can be thought of as
  the average number of bits per coefficient used by an optimal
  quantized estimator, because
  $\varepsilon^{-\frac{2}{2m+1}}$ is asymptotically the number
  of coefficients needed to estimate at the classical minimax rate.
  The curve indicates that with only $2$ bits per
  coefficient, optimal quantized minimax estimation degrades
  by less than a factor of 2 in the constant. With $3$ bits
  per coefficient, the constant is very close to the classical Pinsker
  constant.}
\label{fig:constant}
\end{center}
\end{figure}


In the third regime where the communication budget is insufficient for
the estimator to achieve the optimal rate, we obtain a sub-optimal
rate which no longer depends explicitly on the noise level
$\varepsilon$ of the model. In this regime, quantization error
dominates, and the risk decays at a rate of $B^{-\frac{1}{2m}}$ no
matter how fast $\varepsilon$ approaches zero, as long as
$B\ll\varepsilon^{-\frac{2}{2m+1}}$.  Here the analogue of Pinsker's
constant takes a very simple form.


\vskip10pt
\begin{proof}[Proof of Theorem \ref{thm_lowerbound}]
Consider a Gaussian prior
distribution on $\theta=(\theta_j)_{j=1}^\infty$ 
with $\theta_j\sim\mathcal N(0,\sigma_j^2)$ for $j=1,2,\dots,$
in terms of parameters
$\sigma^2=(\sigma_j^2)_{j=1}^\infty$ to be specified later. One
requirement for the variances is
\begin{equation*}
\sum_{j=1}^\infty a_j^2\sigma_j^2\leq \frac{c^2}{\pi^{2m}}.
\end{equation*}
We denote this prior distribution by $\pi(\theta;\sigma^2)$,
and show in Section~\ref{sec_proof} that it is asymptotically
concentrated on the ellipsoid $\Theta(m,c)$.  Under
this prior the model is
\begin{align*}
\theta_j & \sim\mathcal N(0,\sigma_j^2)\\
Y_j \given \theta_j & \sim\mathcal N(\theta_j,\varepsilon^2),\quad j=1,2,\dots\label{model}
\end{align*}
and 
the marginal distribution of $Y_j$ is thus ${\mathcal N}(0,\sigma^2_j+\eps^2)$.
Following the strategy outlined in Section~\ref{sec:general}, let
$\delta$ denote the posterior mean of $\theta$ given $Y$ under this
prior, and consider the optimization
\begin{align*}
\quad \inf \ &\ \mathbb E\|\delta-\tilde\theta\|^2\\
\text{such that}\ &\ I(\delta;\tilde\theta)\leq B_\epsilon
\end{align*}
where the infimum is over all distributions on $\tilde\theta$ such
that $\theta \to Y \to \tilde\theta$ forms a Markov chain.
Now, the posterior mean satisfies $\delta_j = \gamma_j Y_j$ where 
$\gamma_j = \sigma_j^2 / (\sigma_j^2 + \epsilon^2)$. 
Note that the Bayes risk 
under this prior is
\begin{equation*}
\E\|\theta - \delta\|_2^2 = \sum_{j=1}^\infty \frac{\sigma_j^2
  \eps^2}{\sigma_j^2 + \eps^2}.
\end{equation*}
Define
\begin{equation*}
  \mu_j^2  \triangleq \E(\delta_j - \tilde\theta_j)^2.
\end{equation*}
Then the classical rate distortion argument \cite{Cover:2006} gives that 
\begin{align*}
I(\delta; \tilde\theta) & \geq \sum_{j=1}^\infty I(\gamma_j Y_j; \tilde\theta_j) \\
&  \geq \sum_{j=1}^\infty \frac{1}{2} \log_+ \left(\frac{\gamma_j^2
  (\sigma^2_j +\eps^2)}{\mu_j^2}\right) \\
&  = \sum_{j=1}^\infty \frac{1}{2} \log_+
\left(\frac{\sigma_j^4}{\mu_j^2 (\sigma_j^2 + \eps^2)}\right)
\end{align*}
where $\log_+(x) = \max(\log x, 0)$.  
Therefore, the quantized minimax risk is lower bounded by
\begin{equation*}
R_\eps( m, c, B_\eps) = \inf_{\hat\theta_\eps,C(\hat\theta_\eps)\leq B_\eps}\sup_{\theta\in\Theta(m,c)} \mathbb
E\|\theta-\hat\theta_\eps\|^2
\geq V_\eps(B_\eps,m,c) (1+o(1))
\end{equation*}
where $V_\eps(B_\eps, m, c)$ is the value of the 
optimization
\begin{equation}
\tag{$\mathcal P_1$}\label{P1}
\begin{aligned}
\max_{\sigma^2}\min_{\mu^2}\ &\ \sum_{j=1}^\infty\mu_j^2+\sum_{j=1}^\infty\frac{\sigma_j^2\varepsilon^2}{\sigma_j^2+\varepsilon^2}\\
\text{such that}\ &\ \sum_{j=1}^\infty\frac{1}{2}\log_+\left(\frac{\sigma_j^4}{\mu_j^2(\sigma_j^2+\varepsilon^2)}\right)\leq B_\eps\\
&\ \sum_{j=1}^\infty a_j^2\sigma_j^2\leq \frac{c^2}{\pi^{2m}}
\end{aligned}
\end{equation}
and the $(1+o(1))$ deviation term is analyzed in
the supplementary material.

Observe that the quantity $V_{\varepsilon}(B_\varepsilon,m,c)$ can be upper and lower bounded by 
\begin{equation}\label{vupperlower}
\max\Bigl\{R_{\varepsilon}(m,c),Q_{\varepsilon}(m,c,B_\varepsilon)\Bigr\}
\leq V_\eps(m, c, B_\eps)\\
\leq R_{\eps}(m,c)+ Q_{\varepsilon}(m,c,B_\varepsilon)
\end{equation}
where the estimation error term $R_{\varepsilon}(m,c)$ is the value of the optimization
\begin{equation}
\tag{$\mathcal R_{1}$}\label{eqn_E1}
\begin{aligned}
\max_{\sigma^2}\ &\ \sum_{j=1}^\infty\frac{\sigma_j^2\varepsilon^2}{\sigma_j^2+\varepsilon^2}\\
\text{such that}\ &\ \sum_{j=1}^\infty a_j^2\sigma_j^2\leq \frac{c^2}{\pi^{2m}}
\end{aligned}
\end{equation}
and the quantization error term $Q_{\varepsilon}(m,c,B_\varepsilon)$ is the value of the optimization
\begin{equation}
\tag{$\mathcal Q_{1}$}\label{eqn_Q1}
\begin{aligned}
\max_{\sigma^2}\min_{\mu^2}\ &\ \sum_{j=1}^\infty\mu_j^2\\
\text{such that}\ &\ \sum_{j=1}^\infty\frac{1}{2}\log_+\left(\frac{\sigma_j^4}{\mu_j^2(\sigma_j^2+\varepsilon^2)}\right)\leq B_\eps\\
&\ \sum_{j=1}^\infty a_j^2\sigma_j^2\leq \frac{c^2}{\pi^{2m}}.
\end{aligned}
\end{equation}
The following results specify the leading order asymptotics of these quantities.
\begin{lemma} \label{lem_v1}
As $\eps\to0$,
\begin{equation*}
R_{\eps}(m,c)=\mathsf P_{m,c}\,\varepsilon^{\frac{4m}{2m+1}}(1+o(1)).
\end{equation*}
\end{lemma}
\begin{lemma} \label{lem_v2}
As $\eps\to0$,
\begin{equation}
Q_{\eps}(m,c,B_\eps)\leq \frac{c^2m^{2m}}{\pi^{2m}}B_\varepsilon^{-2m}(1+o(1)).\label{eqn_upperboundQ}
\end{equation}
Moreover, if $B_\varepsilon\varepsilon^{\frac{2}{2m+1}}\to0$ and $B_\varepsilon\to\infty$,
\begin{equation*}
Q_{\eps}(m,c,B_\eps)=\frac{c^2m^{2m}}{\pi^{2m}}B_\varepsilon^{-2m}(1+o(1)).
\end{equation*}
\end{lemma}
\noindent This yields the following closed form upper bound.
\begin{corollary}
Suppose that $B_\varepsilon\to\infty$ and $\varepsilon\to 0$. Then
\begin{equation}
\label{upperbound}
 V_\eps(m, c, B_\eps)\leq \left(\mathsf P_{m,c}\,\varepsilon^{\frac{4m}{2m+1}}+\frac{c^2m^{2m}}{\pi^{2m}}B_\varepsilon^{-2m}\right)(1+o(1)).
\end{equation}
\end{corollary}

In the insufficient regime
$B_\varepsilon\varepsilon^{\frac{2}{2m+1}}\to0$ and
$B_\varepsilon\to\infty$ as $\varepsilon\to 0$,
equation \eqref{vupperlower} and Lemma \ref{lem_v2} show that
\begin{equation*}
V_\eps(m, c, B_\eps)=\frac{c^2m^{2m}}{\pi^{2m}}B_\varepsilon^{-2m}(1+o(1)).
\end{equation*}
Similarly, in the over-sufficient regime
$B_\eps\eps^{\frac{2}{2m+1}}\to\infty$ as $\varepsilon\to 0$,
we conclude that
\begin{equation*}
V_\eps(m, c, B_\eps)=\mathsf P_{m,c}\,\varepsilon^{\frac{4m}{2m+1}}(1+o(1)).
\end{equation*}

We now turn to the sufficient regime $B_\eps \eps^{\frac{2}{2m+1}}\to d$.
We begin by making three observations about the solution to the optimization (\ref{P1}).
First, we note that the series $(\sigma^2_j)_{j=1}^\infty$ that solves
(\ref{P1}) can be assumed to be decreasing. If $(\sigma^2_j)$ were not in decreasing order, we could
rearrange it to be decreasing, and correspondingly rearrange
$(\mu^2_j)$, without violating the constraints or changing the value of the
optimization.  Second,  we note that
given $(\sigma^2_j)$, the optimal $(\mu^2_j)$ is obtained by the ``reverse
water-filling'' scheme \cite{Cover:2006}. Specifically, there exists $\eta > 0$ such that
\begin{equation*}
\mu_j^2=\begin{cases}
\eta & \text{ if } \displaystyle \frac{\sigma_j^4}{\sigma_j^2+\varepsilon^2}\geq \eta\\
\displaystyle 
\frac{\sigma_j^4}{\sigma_j^2+\varepsilon^2}& \text{ otherwise},
\end{cases}
\end{equation*}
where $\eta$ is chosen so that
\begin{equation*}
\frac{1}{2}\sum_{j=1}^\infty\log_+\left(\frac{\sigma_j^4}{\mu_j^2(\sigma_j^2+\varepsilon^2)}\right)\leq B_\eps.
\end{equation*}
Third, there exists an integer $J>0$ such that the optimal series $(\sigma^2_j)$ satisfies
\begin{equation*}
\frac{\sigma_j^4}{\sigma^2_j+\varepsilon^2}\geq \eta,\text{ for } j=1,\dots, J\quad\text{and}\quad\sigma_j^2=0, \text{ for }j>J,
\end{equation*}
where $\eta$ is the ``water-filling level'' for $(\mu^2_j)$.
Using these three observations, the optimization (\ref{P1}) can be reformulated
as
\begin{equation}\label{P2}
\tag{$\mathcal P_2$}
\begin{aligned}
\max_{\sigma^2,J}\ &\ J\eta+\sum_{j=1}^J\frac{\sigma_j^2\varepsilon^2}{\sigma_j^2+\varepsilon^2}\\
\text{such that}\ &\ \frac{1}{2}\sum_{j=1}^J\log_+\left(\frac{\sigma_j^4}{\eta(\sigma_j^2+\varepsilon^2)}\right)= B_\eps\\
&\ \sum_{j=1}^J a_j^2\sigma_j^2\leq \frac{c^2}{\pi^{2m}}\\
&\ (\sigma_j^2) \text{ is decreasing and
}\frac{\sigma_J^4}{\sigma_J^2+\varepsilon^2} \geq \eta.
\end{aligned}
\end{equation}

To derive the solution to (\ref{P2}), we use a continuous approximation of $\sigma^2$, writing
\begin{equation*}
\sigma^2_j=\sigma^2(jh)h^{2m+1}
\end{equation*}
where $h$ is the bandwidth to be specified and $\sigma^2(\cdot)$ is a
function defined on $(0,\infty)$. 
The constraint that $\sum_{j=1}^\infty a_j^2\sigma^2_j\leq \frac{c^2}{\pi^{2m}}$
becomes the integral constraint
\begin{equation*}
\int_0^\infty x^{2m}\sigma^2(x)dx\leq \frac{c^2}{\pi^{2m}}.
\end{equation*}
We now set the bandwidth so that $h^{2m+1}=\varepsilon^2$.  This
choice of bandwidth will balance the two terms in the objective
function, and thus gives the hardest prior distribution. Applying the
above three observations under this continuous approximation, we transform
problem (\ref{P2}) to the following optimization:
\begin{equation}
\tag{$\mathcal P_3$}\label{P3}
\begin{aligned}
\quad \max_{\sigma^2,x_0}\ &\ x_0\eta+\int_0^{x_0}\frac{\sigma^2(x)}{\sigma^2(x)+1}dx\\
\text{such
  that}\ &\ \int_0^{x_0}\frac{1}{2}\log_+\left(\frac{\sigma^4(x)}{\eta(\sigma^2(x)+1)}
\right)= d\\
&\ \int_0^{x_0} x^{2m}\sigma^2(x)dx\leq \frac{c^2}{\pi^{2m}}\\
&\ \sigma^2(x)\text{ is decreasing and }\frac{\sigma^4(x)}{\sigma^2(x)+1}\geq\eta\text{ for all }x\leq x_0.
\end{aligned}
\end{equation}
Note that here we omit the convergence rate $h^{2m}=\varepsilon^{\frac{4m}{2m+1}}$ in the objective function.
Solving the first constraint for $\eta$ yields
\begin{equation}\label{P4}
\begin{aligned}
\quad \max_{\sigma^2,x_0}\ &\ \int_0^{x_0}\frac{\sigma^2(x)}{\sigma^2(x)+1}dx+x_0\exp\left(\frac{1}{x_0}\int_0^{x_0}\log\frac{\sigma^4(x)}{\sigma^2(x)+1}dx-\frac{2d}{x_0}\right)\\
\text{such that}\ &\ \int_0^{x_0} x^{2m}\sigma^2(x)dx\leq \frac{c^2}{\pi^{2m}}\\
&\ \sigma^2(x)\text{ is decreasing}\\
&\ \frac{\sigma^4(x)}{\sigma^2(x)+1}\geq
\exp\left(\frac{1}{x_0}\int_0^{x_0}\log\frac{\sigma^4(x)}{\sigma^2(x)+1}dx-\frac{2d}{x_0}\right)\\
&\  \qquad \text{ for all }x\leq x_0.
\end{aligned}
\tag{$\mathcal P_4$}
\end{equation}

The following is proved using a variational argument in the supplementary material.

\begin{lemma}\label{lem_variational}
The solution to (\ref{P4}) satisfies
\begin{equation*}
\frac{1}{(\sigma^2(x)+1)^2}+\exp\left(\frac{1}{x_0}\int_0^{x_0}\log\frac{\sigma^4(x)}{\sigma^2(x)+1}dx-\frac{2d}{x_0}\right)\frac{\sigma^2(x)+2}{\sigma^2(x)(\sigma^2(x)+1)}=\lambda x^{2m}
\end{equation*}
for some $\lambda > 0$.
\end{lemma}



Fixing $x_0$, the lemma shows that by setting
\begin{equation*}
\alpha=\exp\left(\frac{1}{x_0}\int_0^{x_0}\log\frac{\sigma^4(x)}{\sigma^2(x)+1}dx-\frac{2d}{x_0}\right)
\end{equation*}
we can express $\sigma^2(x)$ implicitly as the unique positive
root of a third-order polynomial in $y$,
\begin{equation*}
\lambda x^{2m}y^3+(2\lambda x^{2m}-\alpha)y^2+(\lambda x^{2m}-3\alpha-1)y-2\alpha.
\end{equation*}
This allows us to compute the values of the optimization numerically.

To summarize, in the regime $B_\eps\eps^{\frac{2}{2m+1}}\to d$ as
$\varepsilon\to 0$,
we obtain 
\begin{equation*}
V_\eps(m, c, B_\eps)=(\mathsf P_{m,c} + \mathsf Q_{m,c,d})\,\varepsilon^{\frac{4m}{2m+1}}(1+o(1)),
\end{equation*}
where we denote by $\mathsf P_{m,c} + \mathsf Q_{m,c,d}$ the 
values of the optimization (\ref{P4}).
\end{proof}

%This result is shown graphically in Figure~\ref{fig:constant}.




