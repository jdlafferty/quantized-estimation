\section{Background and problem formulation} 
We consider the Gaussian white noise model
\begin{equation}
dX(t)=f(t)dt+\varepsilon dW(t), \quad 0\leq t\leq 1,
\end{equation}
where $f$ is an unknown function on $[0,1]$, $W$ is a standard
Wiener process on $[0,1]$ and $\varepsilon$ is the standard deviation 
parameter. Available to the statistician is a sample path $X(t):0\leq t\leq 1$
of this white noise model. The goal is then to estimate the unknown function $f$. 
If the function $f$ is allowed to be chosen without any constraint, 
then this estimation problem can be made arbitrarily hard. 
Therefore, to make the problem interesting and meaningful,
we will assume that $f$ belongs to some function class. 
Specifically, $f$ is assumed to be smooth, 
and a usual class of smooth functions is the Sobolev space defined below.
\begin{definition}
The \emph{Sobolev space} of order $m$ and radius $c$, is defined by
\begin{align*}
W(m,L)=\Big\{&f\in [0,1]\to\mathbb R:f^{(m-1)} \text{ is absolutely continuous and }\\
&\int_0^1(f^{(m)}(x))^2dx\leq c^2\Big\}.
\end{align*}
The \emph{periodic Sobolev space} is defined by
\[
\tilde W(m,L)=\left\{f\in W(m,L):f^{(j)}(0)=f^{(j)}(1),\ j=0,1,\dots,m-1\right\}.
\]
\end{definition}
The minimax risk of estimation associated with Sobolev space is defined as
\[
R_\varepsilon(m,c)=\inf_{\hat f_\varepsilon}\sup_{f\in\tilde W(m,c)}\E\|\hat f_\varepsilon-f\|_2^2
\]
where the infimum is taken over all possible estimator of $f$ based an observation 
from the white noise model with noise level $\varepsilon$. 
Pinsker~\cite{pinsker1980optimal} derived the precise asymptotic rate of this minimax risk:
\begin{equation}
\lim_{\varepsilon\to 0}\varepsilon^{-\frac{4m}{2m+1}}R_\varepsilon(m,c)=
\left(\frac{c^2(2m+1)}{\pi^{2m}}\right)^{\frac{1}{2m+1}}\left(\frac{m}{m+1}\right)^{\frac{2m}{2m+1}}
\triangleq \mathsf P_{m,c}\label{pinsker}
\end{equation}
and the quantity $\mathsf P_{m,c}$ is called Pinsker's constant. 
This result not only shows that the minimax risk goes down as $\varepsilon^{\frac{4m}{2m+1}}$, 
but also gives the exact constant in front of the rate. When the noise level $\varepsilon$ is set to be $1/\sqrt{n}$, we recover the $n^{-\frac{2m}{2m+1}}$ rate as usually seen in other literatures. 

In this traditional setting of minimax theory, any estimator is allowed as long as it is measurable with respect to the data, 
and an ideal one would take infinite number of bits to be represented. 
However, in a setting where communication is constrained or storage is limited,
we need to consider only those estimators that use up to $B$ bits for representation,
where $B$ is a given budget.
To transmit or store the estimate, an \emph{encoder} describes the sourse by an index $\phi(X)$, where
\[
\phi:\mathbb R^{[0,1]}\to\{1,2,\dots,2^B\}\triangleq\mathbb C(B)
\]
is the \emph{encoding function}. The $B$-bit index is then transmitted or stored. A \emph{decoder}, when receiving or retrieving the index, represents the estimates by $\check X$, a recovery based on the index using a \emph{decoding function}
\[
\psi:\{1,2,\dots,2^B\}\to\mathbb R^{[0,1]}.
\]
We write the composition of the encoder and the decoder as $Q=\psi\cdot\phi$,
which we call a quantized estimator.
The procedure is 
We denote by $\mathcal Q(B)$ the collection of all such quantized estimators with budget $B$.
We will let the budget $B$ depend on the noise level $\varepsilon$ and write $B=B_\varepsilon$.
Now we can define the minimax risk for quantized estimation as
\[
R^{\text{q}}_\varepsilon(B_\varepsilon,m,c)=\inf_{Q\in\mathcal Q(B_\varepsilon)}\sup_{f\in\tilde W(m,c)}\E\|Q(X)-f\|_2^2.
\]
As in Pinsker's theorem, we are interested in the asymptotic risk as $\varepsilon\to 0$.

\subsection{Normal means model}
A function $f$ in $L^2[0,1]$ can be represented as an (infinite) linear combination of functions from an orthonormal basis.
Specifically, let $({\varphi_j})_{j=1}^\infty$ be the trigonometric basis, and 
\[
\theta_j=\int_0^1 \varphi_j(t)f(t)dt,\quad j=1,2,\dots,
\]
are the Fourier coefficients.
It has been shown that \cite{} $f=\sum_{j=1}^\infty\theta_j\varphi_j$ belongs to $\tilde W(m,c)$ if and only if 
the Fourier coefficients $\theta$ belong to the \emph{Sobolev ellipsoid} defined as follows:
\[
\Theta(m,c)=\left\{\theta\in\ell^2: \sum_{j=1}^\infty a_j^2\theta_j^2\leq \frac{c^2}{\pi^{2m}}\right\}
\]
where 
\[
a_j=\begin{cases}
j^m,&\text{for even } j,\\
(j-1)^m,&\text{for odd } j.
\end{cases}
\]
Expanded with the same orthonormal basis, the observed path $X(t):t\in[0,1]$ can be converted into an infinite Gaussian sequence
\[
Z_j=\int_0^1 \varphi_j(t)dX(t),\quad j=1,2,\dots,
\]
with $Z_j\sim \mathcal N(\theta_j,\varepsilon^2)$. 
For an estimator $(\hat\theta_j)_{j=1}^\infty$ of $(Z_j)_{j=1}^\infty$, 
an estimate of $f$ can be obtained by
\[
\hat f=\sum_{j=1}^\infty \hat\theta_j\varphi_j
\]
with the squared error $\|\hat f-f\|_2^2=\|\hat\theta-\theta\|_2^2$.
Thus, the problem of estimating a function in Sobolev space is in some sense equivalent to estimating an infinite Gaussian sequence in Sobolev ellipsoid, and the minimax risk can be reformulated as 
\begin{equation}
R^{\text{q}}_\varepsilon(B_\varepsilon,m,c)=\inf_{Q\in\mathcal Q(B_\varepsilon)}\sup_{\theta\in\Omega (m,c)}\E\|Q(Z)-\theta\|_2^2.\label{minimaxriskdef}
\end{equation}

\subsection{Relation to rate distortion theory}

