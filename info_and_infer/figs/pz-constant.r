library(ggplot2)
#library(extrafont)
#loadfonts()
#font_import()
data = read.csv("pzconstant.csv")
cols <- c("black", "red", "red", "blue", "gray")
p = ggplot(data, aes(d, value, colour = L1)) + 
	geom_line(aes(linetype=L1),size=.6) +
        scale_linetype_manual(values=c("solid", "longdash", "dotdash"), name="", labels=c("P+Q","Pinsker")) +
	scale_colour_manual(values = cols, name="", labels=c("P+Q","Pinsker")) +
	scale_y_continuous(limits=c(0,6)) + scale_x_continuous(limits=c(1.1,4.0)) +
	theme(legend.title=element_blank()) + theme_bw() +
	xlab("Bits per coefficient d") + ylab("Leading constant value") + theme(axis.title.x = element_text(family="Times New Roman", face='plain',size=12), axis.title.y = element_text(family="Times New Roman",face='plain',size=12), legend.text = element_text(family="Times New Roman",face='plain',size=12), legend.position="right") + labs(colour="") 

pdf("pzconstant.pdf",height=4,width=7)
print(p)
dev.off()
