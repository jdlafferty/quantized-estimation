\section{Achievability} 
\label{sec:achievability}
We now show that the lower bounds in Theorem
\ref{thm_lowerbound} are achievable by a quantized estimator using a
random coding scheme. The basic idea of our quantized estimation procedure is to
conduct blockwise estimation and quantization together, using a
quantized form of the Stein estimator.

We begin by defining the block system to be used, which is usually referred to as the
\emph{weakly geometric system of blocks} \cite{tsybakov:2008}. 
Let $N_\eps=\lfloor 1/\eps^2\rfloor$ and $\rho_\varepsilon=(\log(1/\varepsilon))^{-1}$. 
Let $J_1,\dots,J_K$ be a partition of the set $\{1,\dots,N_\eps\}$ such that
\begin{align*}
\bigcup_{k=1}^K J_k=\{1,\dots,N_\eps\},\quad J_{k_1}\cap J_{k_2}=\emptyset\text{ for } k_1\neq k_2,\\
\text{and }\min\{j:j\in J_k\}>\max\{j:j\in J_{k-1}\}.
\end{align*}
Let $T_k$ be the cardinality of the $k$th block and suppose that $T_1,\dots,T_k$ satisfy
\begin{equation}\label{wgb}\begin{aligned}
T_1&=\lceil\rho_\varepsilon^{-1}\rceil=\lceil\log(1/\varepsilon)\rceil,\\
T_2&=\lfloor T_1(1+\rho_\varepsilon)\rfloor,\\
&\vdots\\
T_{K-1}&=\lfloor T_1(1+\rho_\varepsilon)^{K-2}\rfloor,\\
T_K&=N_\eps-\sum_{k=1}^{K-1}T_k.
\end{aligned}\end{equation}
For an infinite sequence $x\in\ell_2$, denote by $x_{(k)}$ the vector $(x_j)_{j\in J_k}\in\mathbb R^{T_k}$. We also write $j_k=\sum_{l=1}^{k-1}T_l+1$, which is the smallest index in block $J_k$.

We are now ready to describe the quantized estimation scheme.  We
first give a high-level description of the scheme, and then the
precise specification. In contrast to rate distortion theory, where
the codebook and allocation of the bits are determined once the source
distribution is known, here the codebook and allocation of bits are
adaptive to the data---more bits are used for blocks having larger
signal size.  The first step in our quantization scheme is to
construct a ``base code'' of $2^{B_\eps}$ randomly generated vectors
of maximum block length $T_K$, with $\mathcal N(0,1)$ entries.  The
base code is thought of as a $2^{B_\eps} \times T_K$ random matrix
$\mathcal Z$; it is generated before observing any data, and is shared
between the sender and receiver.  After observing data $(Y_j)$, the
rows of $\mathcal Z$ are apportioned to different blocks $k=1,\ldots,
K$, with more rows being used for blocks having larger estimated
signal size.  To do so, the norm $\|Y_{(k)}\|$ of each block $k$ is
first quantized as a discrete value $\check S_k$.  A subcodebook
$\mathcal Z_k$ is then constructed by normalizing the
appropriate rows and the first $T_k$ columns of the base code,
yielding a collection of random points on the unit
sphere $\mathbb S^{T_k-1}$.  To form a quantized estimate of the
coefficients in the block, the codeword $\check Z_{(k)} \in
\mathcal Z_k$ having the smallest angle to $Y_{(k)}$ is then found.  The appropriate
indices are then transmitted to the receiver.  To decode and
reconstruct the quantized estimate, the receiver first recovers the
quantized norms $(\check S_k)$, which enables reconstruction of the
subdivision of the base code that was used by the encoder. After
extracting for each block $k$ the appropriate row of the base code,
the codeword $\check Z_{(k)}$ is reconstructed, and a
James-Stein type estimator is then calculated.

The quantized estimation scheme is detailed below.
\begin{enumerate}[{\sc Step} 1.]\setlength{\itemsep}{3pt}
\item \textit{Base code generation.}
\begin{enumerate}[{1.}1.]
\item Generate codebook $\mathcal S_k=\bigl\{\sqrt{T_k\eps^2}+i\eps^2:\ i=0,1,\dots,s_k\bigr\}$ where $s_k=\left\lceil\eps^{-2}c(j_k\pi)^{-m}\right\rceil$, for $k=1,\dots, K$.
\item Generate base code $\mathcal Z$, a $2^B\times T_K$ matrix with
  i.i.d.\ $\mathcal N(0,1)$ entries. 
\end{enumerate}
$(\mathcal S_k)$ and $\mathcal Z$ are shared between the encoder and the decoder, before seeing any data. 
\item \textit{Encoding.}
\begin{enumerate}[2.1.]
\item \textit{Encoding block radius.}
For $k=1,\dots, K$, encode \\
$\check S_k=\arg\min\left\{|s-S_k|:s\in\mathcal S_k\right\}$ where
\begin{equation*}
S_k=\begin{cases}
\sqrt{T_k\eps^2}& \text{if }\|Y_{(k)}\|<\sqrt{T_k\eps^2}\\
\sqrt{T_k\eps^2}+c(j_k\pi)^{-m} & \text{if }\|Y_{(k)}\|>\sqrt{T_k\eps^2}+c(j_k\pi)^{-m}\\
\|Y_{(k)}\|& \text{otherwise.}
\end{cases}
\end{equation*}
\item \textit{Allocation of bits.}
Let $(\tilde b_k)_{k=1}^K$ be the solution to the optimization
\begin{equation}\begin{aligned}
\min_{\bar b}\ &\ \sum_{k=1}^K\frac{(\check S_k^2-T_k\eps^2)^2}{\check S_k^2}\cdot 2^{-2 \bar b_k}\label{allocationproblem}\\
\text{such that}\ &\ \sum_{k=1}^K T_k\bar b_k\leq B,\ \bar b_k\geq 0.
\end{aligned}
\end{equation}
\item \textit{Encoding block direction.}
Form the data-dependent codebook as follows. 

Divide the rows of $\mathcal Z$ into blocks of sizes $2^{\lceil T_1\tilde b_1\rceil},\dots, 2^{\lceil T_K\tilde b_K\rceil}$. 
Based on the $k$th block of rows, construct the data-dependent codebook $\tilde {\mathcal Z}_k$ by keeping only the first $T_k$ entries and normalizing each truncated row; specifically, the $j$th row of $\tilde {\mathcal Z}_k$ is given by
\[
\tilde {\mathcal Z}_{k,j}=\frac{\mathcal Z_{i,1:T_k}}{\|\mathcal Z_{i,1:T_k}\|}\in\mathbb S_{T_k-1}
\]
where $i$ is the appropriate row of the base code $\mathcal Z$ and $\mathcal Z_{i,1:t}$ denotes the first $t$ entries of the row vector. A graphical illustration is shown below in Figure \ref{fig_codebook}. 

With this data-dependent codebook, encode 
\[
\check Z_{(k)}=\arg\min\{\langle z,Y_{(k)}\rangle:z\in\tilde{\mathcal Z}_k\}
\]
for $k=1,\dots,K$.

\vskip20pt
\begin{figure}[H]
\begin{center}
\begin{tikzpicture}
\def\wid{5}
\def\len{6}
% whole matrix
\draw [fill=white] (0,0) rectangle (\wid,\len);
% submatrices
\draw [fill=lightgray, opacity=0.5, ultra thin] (0,\len*5/6) rectangle node[color=black,opacity=1] {$\tilde{\mathcal Z}_1$} (\wid*2/5,\len) ;
\draw [color=gray, dotted] (0,\len*5/6) -- (\wid,\len*5/6);
\draw [fill=lightgray, opacity=0.5, ultra thin] (0,\len*4/6) rectangle node[color=black,opacity=1] {$\tilde{\mathcal Z}_2$} (\wid/2,\len*5/6);
\draw [color=gray, dotted] (0,\len*4/6) -- (\wid,\len*4/6);
%\draw [loosely dotted, thick] (\wid*9/20,\len*7/12) -- (\wid*11/20,\len*5/12);
\draw [fill=lightgray, opacity=0.5, ultra thin] (0,\len*3/6) rectangle (\wid*2/3,\len*4/6) ;
\draw [color=gray, dotted] (0,\len*3/6) -- (\wid,\len*3/6);
\draw [fill=lightgray, opacity=0.5, ultra thin] (0,\len*2/6) rectangle (\wid*5/6,\len*3/6) ;
\draw [color=gray, dotted] (0,\len*2/6) -- (\wid,\len*2/6);
\draw [fill=lightgray, opacity=0.5, ultra thin] (0,\len/6) rectangle node[color=black,opacity=1] {$\tilde{\mathcal Z}_K$} (\wid,\len/3);
% horizontal braces
%\draw [decorate,decoration={brace, amplitude=5pt, mirror,raise=0}] (0,\len) -- (\wid*2/5,\len) node [black,midway,yshift=-0.35cm] {\tiny $T_1$};
%\draw [decorate,decoration={brace, amplitude=5pt, mirror,raise=0}] (0,\len*5/6) -- (\wid/2,\len*5/6) node [black,midway,yshift=-0.35cm] {\tiny $T_2$};
%\draw [decorate,decoration={brace, amplitude=5pt, mirror,raise=0}] (0,\len/3) -- (\wid,\len/3) node [black,midway,yshift=-0.35cm] {\tiny $T_K$};
% vertical braces
\node [left] at (0,\len*11/12) {\tiny $2^{\lceil T_1\tilde b_1\rceil}$};
\node [left] at (0,\len*9/12) {\tiny $2^{\lceil T_2\tilde b_1\rceil}$};
\draw [loosely dotted, thick] (-0.6,\len*7/12) -- (-0.6,\len*5/12);
\node [left] at (0,\len*3/12) {\tiny $2^{\lceil T_K\tilde b_K\rceil}$};
\draw [gray] (\wid+0.3,0) -- (\wid+0.3,\len*9/20) ;
\draw [gray] (\wid+0.3,\len) -- (\wid+0.3,\len*11/20) ;
\node at (\wid+0.3,\len/2) {\tiny $2^B$};
\draw [gray] (0,-0.3) -- (\wid*2/5,-0.3) ;
\draw [gray] (\wid,-0.3) -- (\wid*3/5,-0.3) ;
\node at (\wid/2,-.3) {\tiny $T_K$};
\end{tikzpicture}
\end{center}
\caption{An illustration of the data-dependent codebook. The big matrix represents the base code $\mathcal Z$, and the shaded areas are $(\tilde{\mathcal Z}_k)$, sub-matrices of size $T_k\times 2^{\lceil T_k\tilde b_k\rceil}$ with rows normalized.}\label{fig_codebook}
\end{figure}

\end{enumerate}

\item \textit{Transmission}.  Transmit or store $(\check S_k)_{k=1}^K$ and $(\check Z_{(k)})_{k=1}^K$ by their corresponding indices.
\item \textit{Decoding \& Estimation.} 
\begin{enumerate}[4.1.]
\item Recover $(\check S_k)$ based on the transmitted or stored indices and the common codebook $(\mathcal S_k)$.
\item Solve (\ref{allocationproblem}) and get $(\tilde b_k)$. Reconstruct $(\tilde{\mathcal Z}_k)$ using $\mathcal Z$ and $(\tilde b_k)$.
\item Recover $(\check Z_{(k)})$ based on the transmitted or stored indices and the reconstructed codebook $(\tilde{\mathcal Z}_k)$.
\item Estimate $\theta_{(k)}$ by
\begin{equation*}
\check \theta_{(k)}=\frac{\check S_k^2-T_k\eps^2}{\check S_k}\sqrt{1-2^{-2\tilde b_k}}\cdot \check Z_{(k)}.
\end{equation*}
\item Estimate the entire vector $\theta$ by concatenating the
  $\check\theta_{(k)}$ vectors and padding with zeros; thus,
\begin{equation*}
\check\theta = \left(\check\theta_{(1)},\dots,\check\theta_{(K)},0,0,\dots\right).
\end{equation*}
\end{enumerate}
\end{enumerate}

The following theorem establishes the asymptotic optimality of this
quantized estimator. 


\begin{theorem}\label{thm_upperbound} Let $\check\theta$ be the quantized estimator defined above. 
\begin{enumerate}[(i)]
\item If $B\varepsilon^{\frac{2}{2m+1}}\to \infty$, then 
\begin{equation*}
\lim_{\varepsilon\to 0}\,\varepsilon^{-\frac{4m}{2m+1}}\sup_{\theta\in\Theta(m,c)}\mathbb E\|\theta-\check\theta\|^2= \mathsf P_{m,c}.
\end{equation*}
\item If $B\varepsilon^{\frac{2}{2m+1}}\to d$ for some constant $d$ as $\varepsilon\to0$, then
\begin{equation*}
\lim_{\varepsilon\to 0}\,\varepsilon^{-\frac{4m}{2m+1}}\sup_{\theta\in\Theta(m,c)}\mathbb E\|\theta-\check\theta\|^2= \mathsf P_{m,c}+\mathsf Q_{d,m,c}.
\end{equation*}
\item If $B\eps^{\frac{2}{2m+1}}\to 0$ and $B(\log(1/\eps))^{-3}\to\infty$, then 
\begin{equation*}
\lim_{\varepsilon\to 0}\,B^{2m}\sup_{\theta\in\Theta(m,c)}\mathbb E\|\theta-\check\theta\|^2=\frac{c^2m^{2m}}{\pi^{2m}}.
\end{equation*}
\end{enumerate}
The expectations are with respect to the random quantized estimation scheme $Q$ and the distribution of the data. 
\end{theorem}
We pause to make several remarks on this result before outlining the proof.
\begin{remark}
The total number of bits used by this quantized estimation scheme is 
\begin{align*}
\sum_{k=1}^K \lceil T_k \tilde b_k \rceil +\sum_{k=1}^K
\log\lceil\eps^{-2}c(j_k\pi)^{-m}\rceil & \leq \sum_{k=1}^K \lceil T_k
\tilde b_k \rceil +\sum_{k=1}^K\log\lceil\eps^{-2}c\rceil\\
& {} \leq B+ K + 2K\rho_\eps^{-1}+K\log\lceil c\rceil\\
& {} =B+O((\log(1/\eps))^{3}).
\end{align*}
Therefore, as long as $B(\log(1/\eps))^{-3}\to\infty$, the total
number of bits used is asymptotically no more than $B$, the given communication budget.
\end{remark}
\begin{remark}
The quantized estimation scheme does not make essential use of the
parameters of the Sobolev space, namely the smoothness $m$ and the
radius $c$. The only exception is that in Step 1.1 the size of the
codebook $\mathcal S_k$ depends on $m$ and $c$. However, suppose that
we know a lower bound on the smoothness $m$, say $m\geq m_0$, and an
upper bound on the radius $c$, say $c\leq c_0$. By replacing $m$ and
$c$ by $m_0$ and $c_0$ respectively, we make the codebook independent
of the parameters.  We shall assume $m_0 >1/2$, which leads 
to continuous functions.
This modification does not, however, 
significantly increase the number of bits; in fact, the total number of
bits is still $B+O(\rho_\eps^{-3})$. Thus, we can easily make this quantized
estimator minimax adaptive to the class of Sobolev ellipsoids
$\{\Theta(m,c):m\geq m_0,\; c\leq c_0\}$, as long as $B$ grows faster
than $(\log(1/\eps))^3$.  More formally, we have
\begin{corollary}
Suppose that $B_\eps$ satisfies
$B_\eps(\log(1/\eps))^{-3}\to\infty$. Let $\check\theta'$ be the
quantized estimator with the modification described above, which does
not assume knowledge of $m$ and $c$. Then for
$m\geq m_0$ and $c\leq c_0$,
\begin{equation*}
\lim_{\eps\to 0}\frac{\sup_{\theta\in\Theta(m,c)}\mathbb E\|\theta-\check\theta'\|^2}{\inf_{\hat\theta,C(\hat\theta)\leq B}\sup_{\theta\in\Theta(m,c)}\mathbb E\|\theta-\hat\theta\|^2}= 1,
\end{equation*}
where the expectation in the numerator is with respect to the data and
the randomized coding scheme, while the expectation in the denominator is only with respect to the data. 
\end{corollary}

\end{remark}
\begin{remark}
When $B$ grows at a rate comparable to or slower than
$(\log(1/\eps))^3$, the lower bound is still achievable, just no
longer by the quantized estimator we described above. The main reason
is that when $B$ does not grow faster than $\log(1/\eps)^3$, the
block size $T_1=\lceil\log(1/\eps)\rceil$ is too large.  
The blocking needs to be modified to get achievability in this case.
\end{remark}

\begin{remark}
In classical rate distortion \cite{Cover:2006,gallager:1968}, the probabilistic method applied to a
randomized coding scheme shows the existence of a code achieving 
the rate distortion bounds.  According to 
Theorem~\ref{thm_lowerbound}, the expected risk, averaged
over the randomness in the codebook, similarly achieves the quantized
minimax lower bound.  However, note that the average over
the codebook is inside the supremum over the Sobolev space,
implying that the code achieving the bound may vary 
over the ellipsoid.  In other words, while the coding scheme
generates a codebook that is used for different
$\theta$,  it is not known whether there is one code generated by this
randomized scheme that is ``universal,'' and achieves the
risk lower bound with high probability over the ellipsoid.
The existence or non-existence of such ``universal codes'' is an 
interesting direction for further study.
\end{remark}

\paragraph{Proof of Theorem \ref{thm_upperbound}} We now sketch the
proof of Theorem \ref{thm_upperbound}, deferring the full details to
Section \ref{sec_proof}.  To provide only an informal outline of the
proof, we shall write $A_1\approx A_2$ as a shorthand for $A_1=A_2(1+o(1))$,
and $A_1\lesssim A_2$ for $A_1\leq A_2(1+o(1))$, without specifying
here what these $o(1)$ terms are.

To upper bound the risk $\mathbb E\|\check\theta-\theta\|^2$, we adopt
the following sequence of approximations and inequalities. First, we discard the components whose index is greater than $N$ and show that
\begin{align*}
\hspace{-0.5in}&\mathbb E\|\check\theta-\theta\|^2\approx \mathbb E\sum_{k=1}^K\|\check\theta_{(k)}-\theta_{(k)}\|^2.\\
\intertext{Since $\check S_k$ is close enough to $S_k$, we can then safely replace $\check\theta_{(k)}$ by $\hat\theta_{(k)}=\frac{S_k^2-T_k\eps^2}{S_k}\sqrt{1-2^{-2\tilde b_{(k)}}}\cdot \check Z_{(k)}$ and obtain}
&\approx \mathbb E\sum_{k=1}^K\|\hat\theta_{(k)}-\theta_{(k)}\|^2.\\
\intertext{Writing $\lambda_k = \frac{S_k^2-T_k\eps^2}{S_k^2}$, we further decompose the risk into}
&\begin{aligned}&=\mathbb E\sum_{k=1}^K\Big(\|\hat\theta_{(k)}-\lambda_k Y_{(k)}\|^2+\|\lambda_k Y_{(k)}-\theta_{(k)}\|^2\\
&\hspace{1in}+2\langle\hat\theta_{(k)}-\lambda_k Y_{(k)},\lambda_k Y_{(k)}-\theta_{(k)} \rangle\Big).
\end{aligned}\\
\intertext{Conditioning on the data $Y$ and taking the expectation
  with respect to the random codebook yields}
&\lesssim \mathbb E\sum_{k=1}^K\left(\frac{(S_k^2-T_k\eps^2)^2}{S_k^2}2^{-2\tilde b_k}+\|\lambda_k Y_{(k)}-\theta_{(k)}\|^2\right).\\
\intertext{By two oracle inequalities upper bounding the expectations with respect to the data, and the fact that $\tilde b$ is the solution to \eqref{allocationproblem}, }
&\lesssim\min_{b\in\Pi_{\text{blk}}(B)}\sum_{k=1}^K\left(\frac{\|\theta_{(k)}\|^4}{\|\theta_{(k)}\|^2+T_k\varepsilon^2} 2^{-2 \bar b_{k}}+\frac{\|\theta_{(k)}\|^2T_k\varepsilon^2}{\|\theta_{(k)}\|^2+T_k\varepsilon^2}\right).\\
\intertext{Showing that the blockwise constant oracles are almost as good as the monotone oracle, we get for some $B'\approx B$}
&\lesssim\min_{b\in\Pi_{\text{mon}}(B'),\ \omega\in\Omega_{\text{mon}}}\sum_{j=1}^N\left(\frac{\theta_j^4}{\theta_j^2+\varepsilon^2}2^{-2 b_{j}}+(1-\omega_j)^2\theta_j^2+\omega_j^2\varepsilon^2\right),
\end{align*}
where $\Pi_{\text{blk}}(B)$, $\Pi_{\text{mon}}(B)$ are the classes of blockwise constant and monotone allocations of the bits defined in \eqref{eqn_piblk}, \eqref{eqn_pimon}, and $\Omega_{\text{mon}}$ is the class of monotone weights defined in (\ref{eqn_omegamon}).
The proof is then completed by Lemma \ref{lem_equivalence} showing that the last quantity is equal to $V_\eps(m,c,B)$.

\paragraph{Simulation} Here we illustrate the performance of the proposed quantized estimation scheme. 
We choose the Doppler function as the underlying true function in our test case. 
The Doppler function with offset $\alpha$ is defined as 
\[
f_\alpha(x)=\sqrt{x(1-x)}\sin\left(\frac{2.1\pi}{x+\alpha}\right),\quad 0\leq x\leq 1
\]
where $\alpha>0$ is a constant, which to some extent controls the smoothness of the function. 
In our simulations, we set $\alpha$ to be 0.1 and 0.3, and two resulting functions are shown in the top panels in Fig.~\ref{fig:sim}. 
We apply both the blockwise James-Stein estimator, which achieves the minimax risk, as well as the proposed quantized estimator. 
We show in Fig.~\ref{fig:sim} the risks, which are calculated by averaging over 1,000 simulations versus the effective sample size $n=1/\eps^2$.
In both plots, we are able to observe the phase transition for the quantized estimators.
For relatively small values of $n$, all quantized estimators perform similarly despite their different budget. 
Their risks are close to (even smaller than) that of the blockwise James-Stein estimator.
This is the over-sufficient regime, since even the smallest budget suffices to achieve the optimal risk.
As $n$ increases, the curves start to move separately where estimators with smaller budgets lead to worse risks compared to the blockwise James-Stein estimator and the others with larger budgets.
This can be viewed as the sufficient regime -- the risks are still going down, but at a slower rate than optimal. 
Finally, the six quantized estimators all end up in the insufficient regime.
$n$ increases, but their risks stay constant, while the risk of the blockwise James-Stein estimator continues to decrease.



\begin{figure}[t]
\begin{center}
\begin{tabular}{c}
\hskip-5pt
\includegraphics[width=0.95\textwidth]{figs/risk_doppler} \\
\end{tabular}
\caption{Top: plots of two Doppler functions with $\alpha=0.1$ (left) and 0.3 (right).
Bottom: risk versus noise level (or equivalently, sample size) $n=1/\eps^2$
for estimating the two functions with different methods. 
Solid lines represents the risk of blockwise James-Stein estimator, and the dashed ones
are for the proposed quantized estimators with different communication budgets. 
The budgets are 5, 10, 15, 20, 25, and 30 bits, corresponding to the lines from top to bottom.
The risks are calculated by averaging over 1,000 simulations.
Notice that risk is plotted on a log scale. }
\label{fig:sim}
\end{center}
\end{figure}

